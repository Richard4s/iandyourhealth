@extends('layouts.app')

@section('content')
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Register') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('register') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Register') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<!-- Start Page Header Wrapper -->
<div class="page-header-wrapper layout-two">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="page-header-content">
                    <h2>My Account</h2>
                    <nav class="page-breadcrumb">
                        <ul class="d-flex justify-content-center">
                            <li><a href="{{ __('/') }}">Home</a></li>
                            <li><a href="shop.html" class="active">My Account</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header Wrapper -->

<!-- Start My Account Wrapper -->
<div id="my-account-page-wrapper" class="pt-88 pt-md-58 pt-sm-48 pb-50 pb-md-20 pb-sm-10">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!-- Start Login Area Wrapper -->
                <div class="my-account-item-wrapper">
                    <h3>Login</h3>

                    <div class="my-account-form-wrap">
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="single-form-input {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                <label for="login_username">Email Address <sup>*</sup></label>
                                @if ($errors->has('email'))
                                    <span style="color: red !important; display: block !important;" class="invalid-feedback" role="alert">
                                        <strong style="color: red !important; display: block !important;">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <input name="email" value="{{ old('email') }}" required />

                            </div>

                            <div class="single-form-input {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                <label for="login_pwsd">Password <sup>*</sup></label>
                                @if ($errors->has('password'))
                                    <span style="color: red !important; display: block !important;" class="invalid-feedback" role="alert">
                                        <strong style="color: red !important; display: block !important;">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <input type="password" id="login_pwsd" name="password" required />
                            </div>

                            <div class="single-form-input d-flex align-items-center mb-14">
                                <button class="btn btn-black" type="submit">Login</button>

                                <div class="custom-control custom-checkbox ml-20">
                                    <input type="checkbox" class="custom-control-input" id="remember_pwsd">
                                    <label class="custom-control-label" for="remember_pwsd">Remember Me</label>
                                </div>
                            </div>

                            <div class="lost-pswd">
                                <a href="#">Lost your Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Login Area Wrapper -->
            </div>

            <div class="col-md-6">
                <!-- Start Register Area Wrapper -->
                <div class="my-account-item-wrapper mt-sm-34">
                    <h3>Register</h3>

                    <div class="my-account-form-wrap">
                        <form action="{{ route('register') }}" method="POST">
                            @csrf

                            <ul>
                            @if($errors)
                                @foreach($errors as $errorName)

                                        <li>{{ $errorName }}</li>
                                @endforeach
                            @endif



                                    </ul>

                            <div class="single-form-input {{ $errors->has('name') ? ' is-invalid' : '' }}">
                                <label for="reg_username">Full Name</label>
                                <input type="text" name="name" value="{{ old('name') }}" required />
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="single-form-input {{ $errors->has('regEmail') ? ' is-invalid' : '' }}">
                                <label for="regEmail">Email Address <sup>*</sup></label>
                                @if ($errors->has('regEmail'))
                                    <span style="color: red !important; display: block !important;" class="invalid-feedback" role="alert">
                                        <strong style="color: red !important; display: block !important;">{{ $errors->first('regEmail') }}</strong>
                                    </span>
                                @endif
                                <input type="email" name="regEmail" required value="{{ old('regEmail') }}" />
                            </div>

                            <div class="single-form-input {{ $errors->has('regPassword') ? ' is-invalid' : '' }}">
                                <label for="reg_pwsd">Password <sup>*</sup></label>
                                @if ($errors->has('regPassword'))
                                    <span style="color: red !important; display: block !important;" class="invalid-feedback" role="alert">
                                        <strong style="color: red !important; display: block !important;">{{ $errors->first('regPassword') }}</strong>
                                    </span>
                                @endif
                                <input type="password" name="regPassword" required />
                            </div>

                            <div class="single-form-input">
                                <label for="reg_pwsd">Confirm Password <sup>*</sup></label>
                                <input type="password" name="password_confirmation" required />
                            </div>

                            <div class="single-form-input mb-18">
                                <p class="mb-0">Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our
                                    <a href="#">privacy policy</a>.</p>
                            </div>

                            <div class="single-form-input">
                                <button class="btn btn-black" type="submit">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Register Area Wrapper -->
            </div>
        </div>
    </div>
</div>
<!-- End My Account Wrapper -->
@endsection
