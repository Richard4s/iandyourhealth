<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>I And Your Health</title>

    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="{{URL::asset('assets/images/logos.png')}}" type="image/x-icon"/>

    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">


    <!--=== Revolution Slider CSS ===-->
    <link href="{{ URL::asset('assets/css/revslider/settings.css') }}" rel="stylesheet">

    <!--=== Bootstrap CSS ===-->
    <link href="{{ URL::asset('assets/css/vendor/bootstrap.min.css') }}" rel="stylesheet">
    <!--=== Font-Awesome CSS ===-->
    <link href="{{ URL::asset('assets/css/vendor/font-awesome.css') }}" rel="stylesheet">
    <!--=== Dl Icon CSS ===-->
    <link href="{{ URL::asset('assets/css/vendor/dl-icon.css') }}" rel="stylesheet">
    <!--=== Plugins CSS ===-->
    <link href="{{ URL::asset('assets/css/plugins.css') }}" rel="stylesheet">
    <!--=== Helper CSS ===-->
    <link href="{{ URL::asset('assets/css/helper.min.css') }}" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="{{ URL::asset('assets/css/style.min.css') }}" rel="stylesheet">

    <!-- Modernizer JS -->
    <script src="{{ URL::asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="">

{{--<!--== Start PreLoader Wrap ==-->--}}
{{--<div class="preloader-area-wrap">--}}
    {{--<div class="spinner d-flex justify-content-center align-items-center h-100">--}}
        {{--<div class="bounce1"></div>--}}
        {{--<div class="bounce2"></div>--}}
        {{--<div class="bounce3"></div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!--== End PreLoader Wrap ==-->--}}

<!--== Start Header Area ===-->
<header id="header-area" class="fixed-top sticky-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="header-content-wrapper d-flex align-items-center">
                    <div class="header-left-area d-flex align-items-center">
                        <!-- Start Logo Area -->
                        <div class="logo-area">
                            <a href="{{ __('/') }}"><img src="{{URL::asset('assets/images/logos.png')}}" alt="Logo"/></a>
                        </div>
                        <!-- End Logo Area -->

                        <!-- Start Menu Icon -->
                        <div class="menu-icon-wrap">
                            <button class="menu-icon-bar modalActive" data-mfp-src="#left-offside-popup"><i class="dl-icon-menu5"></i></button>
                        </div>
                        <!-- End Menu Icon -->
                    </div>

                    <div class="header-mainmenu-area d-none d-lg-block">
                        <!-- Start Main Menu -->
                        <nav id="mainmenu-wrap">
                            <ul class="nav mainmenu justify-content-center">
                                <li class="dropdown-show"><a @php if(Request::segment(1) == '' || Request::segment(1) == 'home') echo 'class="current"'; else echo ''; @endphp href="{{ __('/') }}">Home</a></li>
                                <li class="dropdown-show"><a @php if(Request::segment(1) == 'about') echo 'class="current"'; else echo ''; @endphp href="{{ route('about') }}">About</a></li>
                                <li><a @php if(Request::segment(1) == 'shop') echo 'class="current"'; else echo ''; @endphp href="{{ route('shop') }}">Shop</a></li>
                                <li><a @php if(Request::segment(1) == 'blog') echo 'class="current"'; else echo ''; @endphp href="{{ route('blog') }}">Blog</a></li>
                                <li><a @php if(Request::segment(1) == 'contact') echo 'class="current"'; else echo ''; @endphp  href="{{ route('contact') }}">Contact</a></li>
                            </ul>
                        </nav>
                        <!-- End Main Menu -->
                    </div>

                    <div class="header-right-area d-flex justify-content-end align-items-center">
                        <button class="search-icon animate-modal-popup" data-mfp-src="#search-box-popup"><i class="dl-icon-search1"></i></button>
                        <ul class="user-area">
                            <li class="dropdown-show">
                                <button><i class="fa fa-user"></i></button>
                                <ul class="dropdown-nav">
                                    @if(Auth::check())
                                        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                    @else
                                        <li><a href="{{ route('register') }}">My Account</a></li>
                                    @endif
                                    @if(Auth::check())
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                           document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form></li>
                                    @endif

                                </ul>
                            </li>
                        </ul>
                        <button class="mini-cart-icon modalActive" data-mfp-src="#miniCart-popup">
                            <i class="fa fa-shopping-cart"></i>
                            <!--To sho-->
                            <span class="cart-count">{{ \Cart::session(session()->get('_token'))->getContent()->count() }}</span>
                            {{--//show well--}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--== End Header Area ===-->
