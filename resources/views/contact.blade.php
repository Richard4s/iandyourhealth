@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

<br><br><br><br>
    <!--== Start Contact Page Wrapper ==-->
    <div id="contact-page-wrapper" class="pt-90 pt-md-60 pt-sm-50 pb-50 pb-md-20 pb-sm-10">
        <div class="contact-page-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-6">
                        <div class="contact-page-form-wrap contact-method">
                            <h3>Get in touch</h3>

                            <div class="contact-form-wrap">
                                <form action="http://demo.devitems.com/veera/veera/assets/php/mail.php" method="post" id="contact-form">
                                    <div class="single-input-item">
                                        <input type="text" name="first_name" placeholder="Your Name *" required />
                                    </div>

                                    <div class="single-input-item">
                                        <input type="email" name="email_address" placeholder="Email address *" required />
                                    </div>

                                    <div class="single-input-item">
                                        <input type="text" name="phone_no" placeholder="Your Phone *" required />
                                    </div>

                                    <div class="single-input-item">
                                        <textarea name="con_message" id="con_message" cols="30" rows="7" placeholder="Message *" required></textarea>
                                    </div>

                                    <button class="btn btn-black">Send Message</button>

                                    <div class="form-message"></div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-6 mt-sm-50">
                        <div class="contact-info-wrapper contact-method">
                            <h3>Contact Info</h3>

                            <div class="contact-info-content">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="single-contact-info">
                                            <h4>Phone Number</h4>
                                            <a href="tel:08123504617">(+234) 8123504617</a>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="single-contact-info mb-0">
                                            <h4>Say Hello</h4>
                                            <a href="mailto:info@renewedlifecare.com"> info@renewedlifecare.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="contact-page-map-area mt-90 mt-md-60 mt-sm-50">
            <div class="map-area-wrapper">
                <div id="map_content" data-lat="23.763491" data-lng="90.431167" data-zoom="6" data-maptitle="HasTech" data-mapaddress="Floor# 4, House# 5, Block# C </br> Banasree Main Rd, Dhaka">
                </div>
            </div>
        </div>
    </div>
    <!--== End Contact Page Wrapper ==-->

@endsection