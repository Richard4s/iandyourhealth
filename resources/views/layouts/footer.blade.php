<!--== Start Footer Section ===-->
<footer id="footer-area">
    <!-- Start Footer Widget Area -->
    <div class="footer-widget-area pt-40 pb-28">
        <div class="container">
            <div class="footer-widget-content">
                <div class="row">
                    <!-- Start Footer Widget Item -->
                    <div class="col-md-4 col-lg-3 order-2 order-lg-1">
                        <div class="footer-widget-item-wrap">
                            <h3 class="widget-title">Contact Us</h3>
                            <div class="widget-body">
                                <div class="contact-text">
                                    <a href="tel:08123504617">(+234) 8123504617</a>
                                    <a href="tel:09064714062">(+234) 9064714062</a>
                                    <a href="tel:09064716853">(+234) 9064716853</a>
                                    <a href="mailto:info@renewedlifecare.com"> info@renewedlifecare.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Footer Widget Item -->

                    <!-- Start Footer Widget Item -->
                    <div class="col-md-4 col-lg-6 order-1 text-left text-lg-center">
                        <div class="row">
                            <div class="col-lg-8 col-xl-6 m-auto">
                                <div class="footer-widget-item-wrap">
                                    <div class="widget-body">
                                        <div class="about-text">
                                            <a href="index.html"><img src="{{URL::asset('assets/images/logos.png')}}" alt="Logo"/></a>
                                            <p>Your health is our priority.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Footer Widget Item -->

                    <!-- Start Footer Widget Item -->
                    <div class="col-md-4 col-lg-3 order-3">
                        <div class="footer-widget-item-wrap">
                            <h3 class="widget-title">Join Our Newsletter</h3>
                            <div class="widget-body">
                                <div class="newsletter-area">
                                    <form action="https://company.us19.list-manage.com/subscribe/post?u=2f2631cacbe4767192d339ef2&amp;id=24db23e68a"
                                          method="post" id="mc-form" class="mc-form">
                                        <input type="email" id="mc-email" autocomplete="off"
                                               placeholder="Enter Email Address" required/>
                                        <button class="btn" type="submit">subscribe</button>
                                    </form>

                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->
                                </div>
                                <div class="footer-social-icons nav mt-18">
                                    <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Footer Widget Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Widget Area -->

    <!-- Start Footer Bottom Area -->
    <div class="footer-bottom-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-5 m-auto order-1">
                    <div class="footer-menu mb-sm-12">
                        <ul class="nav justify-content-center justify-content-md-start">
                            <li><a href="{{ route('about') }}">About Us</a></li>
                            <li><a href="{{ route('blog') }}">Blog</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 col-lg-4 m-auto text-center text-md-left order-3 order-md-2">
                    <div class="copyright-text mt-sm-10">
                        <p>&copy; {{ \Carbon\Carbon::now()->format('Y') }}. All rights reserved.</p>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 m-auto text-center text-md-right order-2 order-md-3">
                    <img src="assets/img/payments.png" alt="Payment Method" />
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Bottom Area -->
</footer>
<!--== End Footer Section ===-->



<!--== Start Search box Wrapper ==-->
<div class="modalSearchBox" id="search-box-popup">
    <div class="modaloverlay"></div>
    <div class="search-box-wrapper">
        <p>Start typing and press Enter to search</p>
        <div class="search-box-form">
            <form action="#" method="POST" class="search-form-area">
                <input type="search"  name="search" id="search" placeholder="Search entire store" />
                <button type="submit" class="btn-search"><i class="dl-icon-search10"></i></button>
            </form>
        </div>
    </div>
</div>
<!--== End Search box Wrapper ==-->

<!--== Start Mini Cart Wrapper ==-->
<div class="mfp-hide modal-minicart" id="miniCart-popup">
    <div class="minicart-content-wrap">
        <h2>Shopping Cart</h2>
        <div class="minicart-product-list">
            <!-- Start Single Product -->

            {{--{{ \Darryldecode\Cart\Facades\CartFacade::session($userId)->isEmpty() }}--}}

            {{--<CartApp :data="{{title}}"></CartApp>--}}

            @if(\Cart::session(session()->get('_token'))->getContent()->count())

                @foreach(\Cart::session(session()->get('_token'))->getContent() as $cartItems)

                    <div class="single-product-item d-flex">
                        <figure class="product-thumb">
                            <a href="single-product.html"><img src="{{ URL::asset('assets/images/').'/'.$cartItems->attributes['image'] }}" alt="{{ $cartItems->name }}"></a>
                        </figure>
                        <div class="product-details">
                            <h2 class="product-title"><a href="single-product.html"></a>{{ $cartItems->name }}</h2>
                            <div class="prod-cal d-flex align-items-center">
                                <span class="quantity">{{ $cartItems->quantity }}</span>
                                <span class="multiplication">&#215;</span>
                                <span class="price">₦{{ $cartItems->price }}</span>
                            </div>
                        </div>
                        <a href="#" class="remove-icon" onclick="removeCart({{ $cartItems->id }})">&#215;</a>
                    </div>



                @endforeach

                    @else
                        <h2 class="product-title">Cart is Empty</h2>
            @endif
        </div>
        <div class="minicart-calculation-wrap d-flex justify-content-between align-items-center">
            <span class="cal-title">Subtotal:</span>
            <span class="cal-amount">₦ {{ \Cart::session(session()->get('_token'))->getSubTotal() }}</span>
        </div>
        <div class="minicart-btn-group mt-38">
            <a href="{{ route('cart') }}" class="btn btn-black ">View Cart</a>
            <a href="{{ route('checkout') }}" class="btn btn-black mt-10">checkout</a>
        </div>
    </div>
</div>
<!--== End Mini Cart Wrapper ==-->


<!--== Start Left Offside Menu Wrapper ==-->
<div class="mfp-hide modalLeftOffside" id="left-offside-popup">
    <div class="leftoffside-content-wrap">
        <nav class="offside-menu">
            <ul class="left-offsidemenu">
                <li><a href="{{ __('/') }}">Home</a></li>
                <li><a href="{{ route('about') }}">About</a></li>
                <li><a href="{{ route('shop') }}">Shop</a></li>
                <li><a href="{{ route('blog') }}">Blog</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
            </ul>
        </nav>
        <div class="offside-text">
            <a href="#"><img src="assets/img/payments.png" alt="Payment Method"></a>
            <p>Your health is our priority.</p>
            <a href="tel:08123504617">(+234) 8123504617</a>
            <a href="tel:09064714062">(+234) 9064714062</a>
            <a href="tel:09064716853">(+234) 9064716853</a>
            <a href="mailto:info@renewedlifecare.com"> info@renewedlifecare.com</a>

            <div class="offset-menu-footer">
                <div class="social-icons nav">
                    <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="#" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                </div>
                <p class="copyright">&copy; {{ \Carbon\Carbon::now()->year }}. All rights reserved.</p>
            </div>
        </div>
    </div>
</div>
<!--== End Left Offside Wrapper ==-->

<!--== Start Quick View Modal Wrapper ==-->
<div class="modal" id="quickViewModal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="dl-icon-close"></i></span>
            </button>
            <div class="modal-body">
                <div class="row">
                    <!-- Start Single Product Thumbnail -->
                    <div class="col-lg-5 col-md-6">
                        <div class="single-product-thumb-wrap p-0 pb-sm-30 pb-md-30">
                            <!-- Product Thumbnail Large View -->
                            <div class="quciview-product-thumb-carousel">
                                <figure class="product-thumb-item">
                                    <img src="assets/img/products/prod-1-1.jpg" alt="Single Product"/>
                                </figure>
                                <figure class="product-thumb-item">
                                    <img src="assets/img/products/prod-1-2.jpg" alt="Single Product"/>
                                </figure>
                                <figure class="product-thumb-item">
                                    <img src="assets/img/products/prod-2-1.jpg" alt="Single Product"/>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Product Thumbnail -->

                    <!-- Start Single Product Content -->
                    <div class="col-lg-7 col-md-6 m-auto">
                        <div class="single-product-content-wrapper">
                            <div class="single-product-details">
                                <h2 class="product-name">Open-knit sweater</h2>
                                <div class="prices-stock-status d-flex align-items-center justify-content-between">
                                    <div class="prices-group">
                                        <del class="old-price">$50.00</del>
                                        <span class="price">$40.00</span>
                                    </div>
                                    <span class="stock-status"><i class="dl-icon-check-circle1"></i> In  Stock</span>
                                </div>
                                <p class="product-desc">Ut enim added minim veniam, quis nostrud exercitation ullamco
                                    ommodo
                                    consequat. Duis aute irure dolor in reprehenderit dolore eu fugiat nulla pariatur.
                                    Excepteur
                                    sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing
                                    elit. Ab dolorem eum labore minima possimus quaerat quod recusandae repellat sequi
                                    ut.</p>

                                <div class="quantity-btn-group d-flex">
                                    <div class="pro-qty">
                                        <input type="text" id="quantity" value="1"/>
                                    </div>
                                    <div class="list-btn-group">
                                        <a href="cart.html" class="btn btn-black">Add to Cart</a>
                                        <a href="wishlist.html" data-toggle="tooltip" data-placement="top"
                                           title="Add to wishlist"><i class="dl-icon-heart2"></i></a>
                                        <a href="compare.html" data-toggle="tooltip" data-placement="top"
                                           title="Add to Compare"><i class="dl-icon-compare2"></i></a>
                                    </div>
                                </div>

                                <div class="find-store-delivery">
                                    <a href="#"><i class="fa fa-map-marker"></i> Find store near you</a>
                                    <a href="#"><i class="fa fa-exchange"></i> Delivery and return</a>
                                </div>
                            </div>

                            <div class="single-product-footer mt-20 pt-20">
                                <div class="prod-footer-right">
                                    <dl class="social-share">
                                        <dt>Share with</dt>
                                        <dd><a href="#"><i class="fa fa-facebook"></i></a></dd>
                                        <dd><a href="#"><i class="fa fa-twitter"></i></a></dd>
                                        <dd><a href="#"><i class="fa fa-pinterest-p"></i></a></dd>
                                        <dd><a href="#"><i class="fa fa-google-plus"></i></a></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Product Content -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Quick View Modal Wrapper ==-->




<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="{{ URL::asset('assets/js/vendor/jquery-3.3.1.min.js') }}"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="{{ URL::asset('assets/js/vendor/jquery-migrate-1.4.1.min.js') }}"></script>
<!--=== Popper Min Js ===-->
<script src="{{ URL::asset('assets/js/vendor/popper.min.js') }}"></script>
<!--=== Bootstrap Min Js ===-->
<script src="{{ URL::asset('assets/js/vendor/bootstrap.min.js') }}"></script>
<!--=== Plugins Js ===-->
<script src="{{ URL::asset('assets/js/plugins.js') }}"></script>
<!--=== Ajax Mail Js ===-->
<script src="{{ URL::asset('assets/js/ajax-mail.js') }}"></script>

<!--=== Active Js ===-->
<script src="{{ URL::asset('assets/js/active.min.js') }}"></script>

<!--=== Revolution Slider Js ===-->
<script src="{{ URL::asset('assets/js/revslider/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/jquery.themepunch.revolution.min.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/revslider/extensions/revolution.extension.video.min.js') }}"></script>


<script src="{{ URL::asset('assets/js/revslider/revslider-active.js') }}"></script>
<script src="{{ URL::asset('assets/js/dropify.js') }}"></script>
<script src="{{ URL::asset('assets/js/custom.js') }}"></script>
<script src="{{ URL::asset('js/app.js') }}"></script>
<script>
    {{--new Vue({--}}
        {{--el: '#cartApp',--}}
        {{--data: {--}}
            {{--title: {{ \Cart::session(session()->get('_token'))->getContent()->count() }}--}}
        {{--}--}}
    {{--})--}}
    function removeCart(id) {
        var ID = id;
        $.ajax({
            headers: {
                'X_CSRF_TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "/removeCart",
            data: {
                'id': ID,
            },
            success: function (data) {
                console.log('Successful' + data);
                window.location.reload();
                document.location.reload(true);
            },
            error: function (data) {
                console.log('Unsuccessful' + data);
            }
        });
        window.location.reload();
        document.location.reload(true);
    }

    function clearCart() {
//        var ID = id;
        $.ajax({
            headers: {
                'X_CSRF_TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "/clearCart",
            data: {
                'id': '1',
            },
            success: function (data) {
                console.log('Successful' + data);
//                window.location.reload();
                document.location.reload(true);
            },
            error: function (data) {
                console.log('Unsuccessful' + data);
            }
        });
//        window.location.reload();
//        document.location.reload(true);
    }
</script>
</body>
</html>