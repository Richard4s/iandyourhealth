@include('layouts.header-cart')
@guest
@else
    @csrf
@endguest

<section>
    @yield('content')
</section>

@include('layouts.footer')
