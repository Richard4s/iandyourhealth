@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <br><br><br><br>
    <!--== Start Checkout Page Wrapper ==-->
    <div id="checkout-page-wrapper" class="pt-90 pt-md-60 pt-sm-50 pb-50 pb-md-20 pb-sm-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="checkout-page-coupon-area">
                        <!-- Checkout Coupon Accordion Start -->
                        <div class="checkoutAccordion" id="checkOutAccordion">
                            <div class="card">
                                <h3><i class="fa fa-info-circle"></i> Have a Coupon? <span data-toggle="collapse"
                                                                                           data-target="#couponaccordion">Click here to Enter your Code</span>
                                </h3>
                                <div id="couponaccordion" class="collapse" data-parent="#checkOutAccordion">
                                    <div class="card-body">
                                        <div class="apply-coupon-wrapper">
                                            <p>If you have a coupon code, please apply it below.</p>
                                            <form action="#" method="post">
                                                <input type="text" placeholder="Enter Your Coupon Code" required/>
                                                <button class="btn btn-black">Apply Coupon</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Checkout Coupon Accordion End -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <!-- Checkout Form Area Start -->
                    <div class="checkout-billing-details-wrap">
                        <h2>Billing Details</h2>
                        <div class="billing-form-wrap">
                            <form action="#" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-input-item">
                                            <label for="f_name" class="required">First Name</label>
                                            <input type="text" id="f_name" placeholder="First Name" required />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="single-input-item">
                                            <label for="l_name" class="required">Last Name</label>
                                            <input type="text" id="l_name" placeholder="Last Name" required />
                                        </div>
                                    </div>
                                </div>

                                <div class="single-input-item">
                                    <label for="email" class="required">Email Address</label>
                                    <input type="email" id="email" placeholder="Email Address" required />
                                </div>

                                <div class="single-input-item">
                                    <label for="com-name">Company Name</label>
                                    <input type="text" id="com-name" placeholder="Company Name" />
                                </div>

                                <div class="single-input-item">
                                    <label for="country" class="required">Country</label>
                                    <select name="country" id="country">
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="India">India</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="England">England</option>
                                        <option value="London">London</option>
                                        <option value="London">London</option>
                                        <option value="Chaina">China</option>
                                    </select>
                                </div>

                                <div class="single-input-item">
                                    <label for="street-address" class="required">Street address</label>
                                    <input type="text" id="street-address" placeholder="Street address Line 1" required />
                                </div>

                                <div class="single-input-item">
                                    <input type="text"  placeholder="Street address Line 2 (Optional)" />
                                </div>

                                <div class="single-input-item">
                                    <label for="town" class="required">Town / City</label>
                                    <input type="text" id="town"  placeholder="Town / City" required />
                                </div>

                                <div class="single-input-item">
                                    <label for="state">State / Divition</label>
                                    <input type="text" id="state"  placeholder="State / Divition" />
                                </div>

                                <div class="single-input-item">
                                    <label for="postcode" class="required">Postcode / ZIP</label>
                                    <input type="text" id="postcode"  placeholder="Postcode / ZIP" required />
                                </div>

                                <div class="single-input-item">
                                    <label for="phone">Phone</label>
                                    <input type="text" id="phone"  placeholder="Phone" />
                                </div>

                                <div class="checkout-box-wrap">
                                    <div class="single-input-item">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="create_pwd">
                                            <label class="custom-control-label" for="create_pwd">Create an account?</label>
                                        </div>
                                    </div>
                                    <div class="account-create single-form-row">
                                        <p>Create an account by entering the information below. If you are a returning customer please login at the top of the page.</p>
                                        <div class="single-input-item">
                                            <label for="pwd" class="required">Account Password</label>
                                            <input type="password" id="pwd"  placeholder="Account Password" required />
                                        </div>
                                    </div>
                                </div>

                                <div class="checkout-box-wrap">
                                    <div class="single-input-item">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="ship_to_different">
                                            <label class="custom-control-label" for="ship_to_different">Ship to a different address?</label>
                                        </div>
                                    </div>
                                    <div class="ship-to-different single-form-row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="single-input-item">
                                                    <label for="f_name_2" class="required">First Name</label>
                                                    <input type="text" id="f_name_2" placeholder="First Name" required />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="single-input-item">
                                                    <label for="l_name_2" class="required">Last Name</label>
                                                    <input type="text" id="l_name_2" placeholder="Last Name" required />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-input-item">
                                            <label for="email_2" class="required">Email Address</label>
                                            <input type="email" id="email_2" placeholder="Email Address" required />
                                        </div>

                                        <div class="single-input-item">
                                            <label for="com-name_2">Company Name</label>
                                            <input type="text" id="com-name_2" placeholder="Company Name" />
                                        </div>

                                        <div class="single-input-item">
                                            <label for="country_2" class="required">Country</label>
                                            <select name="country" id="country_2">
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="India">India</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="England">England</option>
                                                <option value="London">London</option>
                                                <option value="London">London</option>
                                                <option value="China">China</option>
                                                <option value="Nigeria">Nigeria</option>
                                            </select>
                                        </div>

                                        <div class="single-input-item">
                                            <label for="street-address_2" class="required">Street address</label>
                                            <input type="text" id="street-address_2" placeholder="Street address Line 1" required />
                                        </div>

                                        <div class="single-input-item">
                                            <input type="text"  placeholder="Street address Line 2 (Optional)" />
                                        </div>

                                        <div class="single-input-item">
                                            <label for="town_2" class="required">Town / City</label>
                                            <input type="text" id="town_2"  placeholder="Town / City" required />
                                        </div>

                                        <div class="single-input-item">
                                            <label for="state_2">State / Divition</label>
                                            <input type="text" id="state_2"  placeholder="State / Divition" />
                                        </div>

                                        <div class="single-input-item">
                                            <label for="postcode_2" class="required">Postcode / ZIP</label>
                                            <input type="text" id="postcode_2"  placeholder="Postcode / ZIP" required />
                                        </div>
                                    </div>
                                </div>

                                <div class="single-input-item">
                                    <label for="ordernote">Order Note</label>
                                    <textarea name="ordernote" id="ordernote" cols="30" rows="3" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-xl-5 ml-auto">
                    <!-- Checkout Page Order Details -->
                    <div class="order-details-area-wrap">
                        <h2>Your Order</h2>

                        <div class="order-details-table table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Products</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(\Cart::session(session()->get('_token'))->getContent()->count())

                                    @foreach(\Cart::session(session()->get('_token'))->getContent() as $cartItems)
                                        <tr class="cart-item">
                                            <td><span class="product-title">{{ $cartItems->name }}</span> <span class="product-quantity">&#215;  {{ $cartItems->quantity }}</span></td>
                                            <td>{{ $cartItems->price }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr class="cart-subtotal">
                                    <th>Subtotal</th>
                                    <td>₦ {{ \Cart::session(session()->get('_token'))->getSubTotal() }}</td>
                                </tr>

                                <tr class="final-total">
                                    <th>Total</th>
                                    <td><span class="total-amount">₦ {{ \Cart::session(session()->get('_token'))->getTotal() }}</span></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="order-details-footer">
                            <p>Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our
                                <a href="#" class="text-danger">privacy policy</a>.</p>
                            <div class="custom-control custom-checkbox mt-10">
                                <input type="checkbox" id="privacy" class="custom-control-input" required />
                                <label for="privacy" class="custom-control-label">I have read and agree to the website terms and conditions</label>
                            </div>

                            <button class="btn btn-full btn-black mt-26">Place Order</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Checkout Page Wrapper ==-->

@endsection