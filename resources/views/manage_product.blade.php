@extends('layouts.dash-app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="wrapper">
        <div class="container-fluid"><!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group float-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Fegor shoes</a></li>
                                <li class="breadcrumb-item active">Manage Products</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Manage Products</h4></div>
                </div>
            </div><!-- end page title end breadcrumb -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right">
                        <a href="{{ route('add_product') }}"><button type="button" class="btn btn-success waves-effect waves-light">Add Products</button></a>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-xl-12">
                    <div class="card m-b-30">
                        <div class="card-body"><h4 class="mt-0 header-title">Categories</h4>
                            <table id="product-datatable" class="table table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th># S/N</th>
                                    <th>Product Name</th>
                                    <th>Product Price</th>
                                    <th>Product Category</th>
                                    <th>First Image</th>
                                    <th>Second Image</th>
                                    <th>Product Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($info as $actinfo)

                                    <tr>
                                        <td>{{ $actinfo->id }}</td>
                                        <td>{{ $actinfo->productName }}</td>
                                        <td>{{ $actinfo->productPrice }}</td>
                                        <td>{{ DB::table('categories')->selectRaw('categories.categoryName')->whereRaw('categories.id = ?', [$actinfo->productCategory])->limit(1)->pluck('categoryName')->first() }}</td>
                                        <td><img width="100" src="{{ URL::asset('assets/images/').'/'.$actinfo->productImage }}" /></td>
                                        <td><img width="100" src="{{ URL::asset('assets/images/').'/'.$actinfo->secondImage }}" /></td>
                                        <td>{{ $actinfo->productDescription }}</td>
                                        <td>
                                            <a href="{{ __('edit_product') }}/{{ $actinfo->id }}"><button type="button" class="btn btn-info waves-effect waves-light"><i class="dripicons-document-edit"></i></button></a>
                                            <a><button type="button" id="product-delete" class="btn btn-danger waves-effect waves-light"><i class="dripicons-trash"></i></button></a>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- end row -->
            <!-- end row -->

@endsection