@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <br><br><br><br>
    <!--== Start Coming Soon Page Wrapper ==-->
    <div id="coming-soon-page-wrapper">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-12 my-auto text-center">
                    <div class="coming-soon-content-wrap">
                        <h2>Coming Very Soon</h2>
                        <p class="col-md-8 col-lg-6 m-auto">Your health is our priority </p>

                        <div class="coming-soon-count-down mt-70 mt-sm-30">
                            <div class="sale-countdown d-flex justify-content-center" data-date="03/04/2019"></div>
                        </div>

                        <div class="newsletter-content-wrap mt-80 mt-sm-30 col-md-8 col-lg-6 m-auto">
                            <div class="newsletter-area">
                                <form action="https://company.us19.list-manage.com/subscribe/post?u=2f2631cacbe4767192d339ef2&amp;id=24db23e68a"
                                      method="post" id="mc-form" class="mc-form">
                                    <input type="email" id="mc-email" autocomplete="off"
                                           placeholder="Enter Email Address" required/>
                                    <button class="btn" type="submit">subscribe</button>
                                </form>

                                <!-- mailchimp-alerts Start -->
                                <div class="mailchimp-alerts">
                                    <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                    <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                    <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                </div>
                                <!-- mailchimp-alerts end -->
                            </div>
                        </div>

                        <a href="{{ __('/') }}" class="btn btn-black mt-96 mt-sm-30">Back to Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Coming Soon Page Wrapper ==-->

@endsection