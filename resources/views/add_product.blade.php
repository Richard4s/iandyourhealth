@extends('layouts.dash-app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="wrapper">
        <div class="container-fluid"><!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group float-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Fegor shoes</a></li>
                                <li class="breadcrumb-item active">Add Product</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Add Product</h4></div>
                </div>
            </div><!-- end page title end breadcrumb -->

            <br>



            <form method="post" action="{{ route('add_product') }}" id="" enctype="multipart/form-data">
                @csrf
                <div class="row justify-content-center" >
                    <div class="col-md-9">
                        <div class="card m-b-30">
                            <div class="card-body"><h4 class="mt-0 header-title">Add Product</h4>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Product Name</label>
                                    <div class="col-sm-8">
                                        <input class="form-control{{ $errors->has('productName') ? ' is-invalid' : '' }}" id="productName" name="productName" type="text" value="{{ old('productName') }}" />
                                        @if ($errors->has('productName'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('productName') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Product Price</label>
                                    <div class="col-sm-8">
                                        <input class="form-control{{ $errors->has('productPrice') ? ' is-invalid' : '' }}" name="productPrice" type="text" value="{{ old('productPrice') }}" />
                                        @if ($errors->has('productPrice'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('productPrice') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Product Category</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="productCategory">
                                            @foreach($catName as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->categoryName }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Product Image</label>
                                    <div class="col-sm-8">
                                        <input name="productImage" type="file" class="dropify" data-height="100" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Second Product Image</label>
                                    <div class="col-sm-8">
                                        <input name="secondImage" type="file" class="dropify" data-height="100" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Product Description</label>
                                    <div class="col-sm-8">
                                        <textarea rows="9" class="form-control{{ $errors->has('productDescription') ? ' is-invalid' : '' }}" name="productDescription" type="text" >
                                            {{ old('productDescription') }}
                                        </textarea>
                                        @if ($errors->has('productDescription'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('productDescription') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button type="" id="submit-all" class="btn btn-success waves-effect waves-light">Add Product</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- end col --></div>

            </form>
        </div>
    </div>


@endsection