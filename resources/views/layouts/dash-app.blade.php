@include('layouts.dash-header')
<section>
    @yield('content')
</section>
@include('layouts.dash-footer')
