@extends('layouts.app')

    @section('content')
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <br><br><br><br>
    <!-- Start Page Header Wrapper -->
    <div class="page-header-wrapper layout-two">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="page-header-content">
                        <h2>About Us</h2>
                        <nav class="page-breadcrumb">
                            <ul class="d-flex justify-content-center">
                                <li><a href="{{ __('/') }}">Home</a></li>
                                <li><a href="{{ route('about') }}" class="active">About Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header Wrapper -->

    <!--== Start About Page Wrapper ==-->
    <div id="about-page-wrapper" class="pt-90 pt-md-60 pt-sm-50 pb-50 pb-md-20 pb-sm-10">
        <div class="about-content-wrap mb-84 mb-md-54 mb-sm-46">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mb-sm-28 mb-md-24">
                        <div class="about-thumbnail">
                            <img src="{{ URL::asset('assets/images/renewed-life-care.jpg') }}" alt="About Thumbnail"/>
                        </div>
                    </div>

                    <div class="col-lg-6 ml-auto my-auto">
                        <div class="about-content">
                            <h2>WELCOME TO RENEWED LIFE</h2>
                            <p>RENEWED LIFE CARE Ltd is located in Winchester VA USA. Dedicating it's self to all kind of research and application of natural
                                plants active ingredient. Under the guidance of business phyilosophy that "benefit all beings, spread health". It used the modern
                                high and new technology to develop health care products. </p>

                            <p>RENEWED LIFE CARE Ltd subsidiary of National Fruit Company INC. Manufacturaers & markets apple base products, which was founded
                                in 1908 by David Gum and Paige Gum.</p>
                            <p>RENEWED LIFE CARE Ltd came into Nigeria as a Multi Level Marketing Company in the year 2017.
                                Adhering to the brand concept for “better health and greater happiness now and always”.</p>

                            <p>We provide an "efficient product, as we aim to serve our customer to the fullest.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="why-work-us mb-90 mb-md-60 mb-sm-50">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 order-1 order-lg-0">
                        <div class="why-work-content">
                            <h2>Your health is our priority</h2>
                            <p>We concentrate on using bio-technology to develop the daily natural herbal products to enter the African market
                                through the direct selling model, effectively improving African people`s health and quality of life.</p>

                            <div class="why-work-us-accordion brand-accordion mt-24">
                                <div class="accordion" id="WorkWithAccordion">
                                    <!-- Start Single Accordion Wrap -->
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn" type="button" data-toggle="collapse"
                                                        data-target="#collapseOne" aria-expanded="true">Better health and greater happiness now and always
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapseOne" class="collapse show" data-parent="#WorkWithAccordion">
                                            <div class="card-body">
                                                <p>RENEWED LIFE CARE has create a competent direct selling team with a rich experience for Africa market,
                                                    taking full advantage of the independent product features, leading technology with a brand strategy
                                                    construction to improve the service competition with the new techniques of bio-technology and application
                                                    to strengthen the MLM investment, with the technology application and independent innovation that is used
                                                    to upgrade product and to expand the range of application of products, to also promote the competitiveness
                                                    of the market and customers recognition for becoming the most competitive supplier in African continents.
                                                    Adhering to the brand concept for “better health and greater happiness now and always” -Leo Martins -- CEO --</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single Accordion Wrap -->



                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6 ml-auto order-0 order-lg-1">
                        <figure class="about-pic mb-sm-30 mb-md-30">
                            <img src="assets/img/about2-bg1.jpg" class="w-100" alt="About Image"/>
                        </figure>
                    </div>
                </div>
            </div>
        </div>

        <div class="provide-best-service">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mb-sm-30 mb-md-30">
                        <div class="best-service-content">
                            <h2>BRIEF INTRODUCTION OF THE COMPANY</h2>
                            <p>National Fruits Company INC have been processing and marketing quality Apple product since 1908 with strong heritage
                                and generations of traditions. we take pride in expanding our orchands for the next generations. On March 1, 1909 the
                                young company began marketing it's vinegar in glass package which consumers enjoyed. In 1926 to 1948 Apple Cider Vinegar became
                                a major product at the Winchester plant. Since then till date Apple Cider Vinegar is the most consumable product and also treatment
                                for varieties of different kind of diseases. Apple Cider Vinegar is manufactured with high quality with natural ingredient
                                and no artificial, coloring, sugar and preservative. It is also raw and unfiltered. Apple Cider Vinegar is effective and safe
                                for consumption. It also contains with mother of Vinegar which has cobweb-like appearance and can make the Vinegar look slightly
                                congealed. National Fruits Products company has spread into many countries such as Spain, Italy, Canada, Colombia, Mexico, Japan,
                                Brazil, Greece, Costarica, Australia, Jamaica, Malaysia etc. Once again welcome to the family of National Fruits Products company
                                INC as we at the firm would say, "from our house to yours.</p>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End About Page Wrapper ==-->

    @endsection