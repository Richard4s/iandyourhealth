@extends('layouts.dash-app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="wrapper">
        <div class="container-fluid"><!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group float-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Fegor shoes</a></li>
                                <li class="breadcrumb-item active">Add Category</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Add Category</h4></div>
                </div>
            </div><!-- end page title end breadcrumb -->

            <br>



            <form method="post" action="{{ __('add_category') }}">
                @csrf
            <div class="row justify-content-center" >
                <div class="col-md-9">
                    <div class="card m-b-30">
                        <div class="card-body"><h4 class="mt-0 header-title">Add Category</h4>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">Category Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('categoryName') ? ' is-invalid' : '' }}" name="categoryName" type="text" value="{{ old('categoryName') }}" />
                                    @if ($errors->has('categoryName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('categoryName') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success waves-effect waves-light">Add Category</button>
                            </div>
                        </div>
                    </div>
                </div><!-- end col --></div>

            </form>
        </div>
    </div>


@endsection