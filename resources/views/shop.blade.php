@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <br><br><br><br>
    <!-- Start Page Header Wrapper -->
    <div class="page-header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="page-header-content">
                        <h2>Shop Sidebar</h2>
                        <nav class="page-breadcrumb">
                            <ul class="d-flex justify-content-center">
                                <li><a href="{{ __('/') }}">Home</a></li>
                                <li><a href="{{ route('shop') }}" class="active">Shop</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header Wrapper -->

    <!--== Start Shop Page Wrapper ==-->
    <div id="shop-page-wrapper" class="pt-86 pt-md-56 pt-sm-46 pb-50 pb-md-20 pb-sm-10">
        <div class="container">
            <div class="row">
                <!-- Start Sidebar Area Wrapper -->
                <div class="col-lg-3 order-last order-lg-first mt-md-54 mt-sm-44">
                    <div class="sidebar-area-wrapper">
                        <!-- Start Single Sidebar -->
                        <div class="single-sidebar-wrap">
                            <h3 class="sidebar-title">Categories</h3>
                            <div class="sidebar-body">
                                <ul class="sidebar-list">
                                    @foreach($cat as $category)
                                        <li><a href="/shop/{{ $category->id }}">{{ $category->categoryName }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Sidebar -->
                    </div>
                </div>
                <!-- End Sidebar Area Wrapper -->

                <!-- Start Shop Page Product Area -->
                <div class="col-lg-9">
                    <!-- Start Product Config Area -->
                    <div class="product-config-area d-md-flex justify-content-between align-items-center">
                        <div class="product-config-left d-sm-flex">
                            <p>Showing 1–12 of 25 results</p>
                            <ul class="product-show-quantity nav mt-xs-14">
                                <li>Show</li>
                                <li><a href="#" class="active">12</a></li>
                                <li><a href="#">15</a></li>
                                <li><a href="#">30</a></li>
                            </ul>
                        </div>

                        <div class="product-config-right d-flex align-items-center mt-sm-14">
                            <ul class="product-view-mode">
                                <li data-viewmode="grid-view" class="active"><i class="fa fa-th"></i></li>
                                <li data-viewmode="list-view"><i class="fa fa-list"></i></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Product Config Area -->

                    <!-- Start Product Wrapper -->
                    <div class="shop-page-products-wrapper mt-44 mt-sm-30">
                        <div class="products-wrapper products-on-column">
                            <div class="row">
                                <!-- Start Single Product -->
                                @foreach($prod as $product)
                                    <div class="col-lg-4 col-sm-6">
                                        <div class="single-product-wrap">
                                            <!-- Product Thumbnail -->
                                            <figure class="product-thumbnail">
                                                <a href="single-product.html" class="d-block">
                                                    <img class="primary-thumb" src="{{ URL::asset('assets/images/').'/'.$product->productImage }}"
                                                         alt="Product"/>
                                                    <img class="secondary-thumb" src="{{ URL::asset('assets/images/').'/'.$product->secondImage }}"
                                                         alt="Product"/>
                                                </a>
                                                <figcaption class="product-hvr-content">
                                                    <a href="#" class="btn btn-black btn-addToCart addToCart">Add to Cart <span class="productID" style="display: none;">{{ $product->id }}</span></a>
                                                    <div class="prod-btn-group">
                                                        @php
                                                            $name = explode(" ", $product->productName);
                                                        @endphp
                                                <span data-toggle="tooltip" data-placement="top" title="Quick Shop"><button
                                                            data-toggle="modal" data-target="#{{ $name[0] }}"><i
                                                                class="dl-icon-view"></i></button></span>
                                                    </div>
                                                </figcaption>
                                            </figure>

                                            <!-- Product Details -->
                                            <div class="product-details">
                                                <h2 class="product-name"><a href="single-product.html">{{ $product->productName }}</a></h2>
                                                <div class="product-prices">
                                                    <span class="price">₦{{ $product->productPrice }}</span>
                                                </div>
                                                <div class="list-view-content">
                                                    <p class="product-desc">{{ $product->productDescription }}</p>

                                                    <div class="list-btn-group mt-30 mt-sm-14">
                                                        <a href="" class="btn btn-black addToCart">Add to Cart <span class="productID" style="display: none;">{{ $product->id }}</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single Product -->

                                    <!--== Start Quick View Modal Wrapper ==-->
                                    <div class="modal" id="{{ $name[0] }}" tabindex="-1">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true"><i class="dl-icon-close"></i></span>
                                                </button>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <!-- Start Single Product Thumbnail -->
                                                        <div class="col-lg-5 col-md-6">
                                                            <div class="single-product-thumb-wrap p-0 pb-sm-30 pb-md-30">
                                                                <!-- Product Thumbnail Large View -->
                                                                <div class="quciview-product-thumb-carousel">
                                                                    <figure class="product-thumb-item">
                                                                        <img src="{{ URL::asset('assets/images/').'/'.$product->productImage }}" alt="Single Product"/>
                                                                    </figure>
                                                                    <figure class="product-thumb-item">
                                                                        <img src="{{ URL::asset('assets/images/').'/'.$product->secondImage }}" alt="Single Product"/>
                                                                    </figure>
                                                                    <figure class="product-thumb-item">
                                                                        <img src="{{ URL::asset('assets/images/').'/'.$product->secondImage }}" alt="Single Product"/>
                                                                    </figure>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Product Thumbnail -->

                                                        <!-- Start Single Product Content -->
                                                        <div class="col-lg-7 col-md-6 m-auto">
                                                            <div class="single-product-content-wrapper">
                                                                <div class="single-product-details">
                                                                    <h2 class="product-name">{{ $product->productName }}</h2>
                                                                    <div class="prices-stock-status d-flex align-items-center justify-content-between">
                                                                        <div class="prices-group">
                                                                            <span class="price">₦{{ $product->productPrice }}</span>
                                                                        </div>
                                                                        <span class="stock-status"><i class="dl-icon-check-circle1"></i> In  Stock</span>
                                                                    </div>
                                                                    <p class="product-desc">{{ $product->productDescription }}</p>

                                                                    <div class="quantity-btn-group d-flex">
                                                                        <div class="pro-qty">
                                                                            <input type="text" id="quantity" value="1"/>
                                                                        </div>
                                                                        <div class="list-btn-group">
                                                                            <a href="#" class="btn btn-black addToCart">Add to Cart <span class="productID" style="display: none;">{{ $product->id }}</span></a>
                                                                        </div>
                                                                    </div>

                                                                    <div class="find-store-delivery">
                                                                        <a href="#"><i class="fa fa-map-marker"></i> Find store near you</a>
                                                                        <a href="#"><i class="fa fa-exchange"></i> Delivery and return</a>
                                                                    </div>
                                                                </div>

                                                                <div class="single-product-footer mt-20 pt-20">
                                                                    <div class="prod-footer-right">
                                                                        <dl class="social-share">
                                                                            <dt>Share with</dt>
                                                                            <dd><a href="#"><i class="fa fa-facebook"></i></a></dd>
                                                                            <dd><a href="#"><i class="fa fa-twitter"></i></a></dd>
                                                                            <dd><a href="#"><i class="fa fa-pinterest-p"></i></a></dd>
                                                                            <dd><a href="#"><i class="fa fa-google-plus"></i></a></dd>
                                                                        </dl>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Product Content -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--== End Quick View Modal Wrapper ==-->
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- End Product Wrapper -->

                    <!-- Page Pagination Start  -->
                    <div class="page-pagination-wrapper mt-70 mt-md-50 mt-sm-40">
                        <nav class="page-pagination">
                            <ul class="pagination justify-content-center">
                                <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                <li><a href="#" class="active">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                    <!-- Page Pagination End  -->
                </div>
                <!-- End Shop Page Product Area -->
            </div>
        </div>
    </div>
    <!--== End Shop Page Wrapper ==-->

@endsection