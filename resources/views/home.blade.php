@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif



    <!--== Start Slider Area ==-->
    <div class="slider-area-wrapper">
        <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="home-01"
             data-source="gallery">
            <!-- START REVOLUTION SLIDER 5.4.7 fullwidth mode -->
            <div id="rev_slider_home1" class="rev_slider fullwidthabanner" data-version="5.4.7">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-7" data-transition="random-premium" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off"
                        data-title="Slide">
                        <!-- MAIN IMAGE -->
                        <img src="assets/img/rev-slider/home1/new-m1-s1.jpg" alt="" data-bgposition="right center"
                             data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg"
                             data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption rev_group" id="slide-7-layer-1" data-x="['left','left','right','left']"
                             data-hoffset="['1080','457','109','0']" data-y="['middle','middle','middle','middle']"
                             data-voffset="['0','-35','0','0']" data-width="['526','526','334','311']"
                             data-height="['241','241','227','231']" data-whitespace="nowrap" data-type="group"
                             data-responsive_offset="on"
                             data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                             data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             style="z-index: 7; min-width: 526px; max-width: 526px; max-width: 241px; max-width: 241px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption   tp-resizeme" id="slide-7-layer-2"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none"
                                 data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                                 data-frames='[{"delay":"+290","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 8; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 700; color: #8d8d8d; letter-spacing: 2px;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption   tp-resizeme" id="slide-7-layer-3"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','15']"
                                 data-y="['top','top','top','top']" data-voffset="['20','20','32','30']"
                                 data-fontsize="['48','48','30','28']" data-lineheight="['70','70','40','36']"
                                 data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":"+770","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 9; white-space: nowrap; font-size: 48px; line-height: 70px; font-weight: 400; color: #262626; letter-spacing: 0px;">
                                Nature Grade CDB
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption tp-resizeme" id="slide-7-layer-4"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['109','109','95','104']"
                                 data-fontsize="['24','24','24','20']" data-lineheight="['33','33','33','30']"
                                 data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":"+1380","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 10; white-space: nowrap; font-size: 24px; line-height: 33px; font-weight: 400; color: #262626;">
                            </div>

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption rev-btn " id="slide-7-layer-5" data-x="['left','left','left','center']"
                                 data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                                 data-voffset="['171','171','167','171']" data-width="none" data-height="none"
                                 data-whitespace="nowrap" data-type="button"
                                 data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"\/shop","delay":""}]'
                                 data-responsive_offset="on" data-responsive="off"
                                 data-frames='[{"delay":"+2170","speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(38,38,38);bw:2 2 2 2;"}]'
                                 data-margintop="[,,,]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[15,15,15,15]" data-paddingright="[45,45,45,45]"
                                 data-paddingbottom="[15,15,15,15]" data-paddingleft="[45,45,45,45]"
                                 style="z-index: 11; white-space: nowrap; font-size: 13px; line-height: 21px; font-weight: 400; color: #262626;background-color:rgba(38,38,38,0);border-color:rgb(38,38,38);border-style:solid;border-width:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                Shop Now
                            </div>
                        </div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption   tp-resizeme" id="slide-7-layer-8" data-x="['left','left','left','left']"
                             data-hoffset="['300','-50','-304','167']" data-y="['top','bottom','bottom','bottom']"
                             data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                             data-type="image" data-responsive_offset="on"
                             data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                             style="z-index: 5;">
                            <div class="rs-looped rs-pulse" data-easing="" data-speed="10" data-zoomstart="1"
                                 data-zoomend="1.1"><img src="{{URL::asset('assets/images/appleveg.jpg')}}" alt=""
                                                         data-ww="['639px','464px','580px','440px']"
                                                         data-hh="['1060px','771px','963px','730px']" data-no-retina></div>
                        </div>

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption   tp-resizeme" id="slide-7-layer-6"
                             data-x="['center','center','center','center']" data-hoffset="['0','-91','-93','39']"
                             data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','49','17']"
                             data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                             data-basealign="slide" data-responsive_offset="on"
                             data-frames='[{"delay":2700,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                             style="z-index: 6;">
                            <div class="rs-looped rs-rotate" data-easing="Power0.easeIn" data-startdeg="-20"
                                 data-enddeg="20" data-speed="10" data-origin="50% 50%"><img
                                        src="assets/img/rev-slider/home1/m1-s1-1.png" alt=""
                                        data-ww="['496px','288px','256px','222px']" data-hh="['320px','186px','165px','143px']"
                                        data-no-retina></div>
                        </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-8" data-transition="random-premium" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1=""
                        data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
                        data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/img/rev-slider/transparent.png" data-bgcolor='#fdf1e8'
                             style='background:#fdf1e8' alt="" data-bgposition="right center" data-bgfit="cover"
                             data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 8 -->
                        <div class="tp-caption     rev_group" id="slide-8-layer-1" data-x="['left','left','left','center']"
                             data-hoffset="['1080','457','324','-49']" data-y="['middle','middle','middle','middle']"
                             data-voffset="['0','-35','-23','-162']" data-width="['638','533','411','373']"
                             data-height="['240','236','225','231']" data-whitespace="nowrap" data-type="group"
                             data-responsive_offset="on"
                             data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                             data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             style="z-index: 7; min-width: 638px; max-width: 638px; max-width: 240px; max-width: 240px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                            <!-- LAYER NR. 9 -->
                            <div class="tp-caption   tp-resizeme" id="slide-8-layer-2"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none"
                                 data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                                 data-frames='[{"delay":"+290","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 8; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 700; color: #8d8d8d; letter-spacing: 2px;">
                                NEW NOW
                            </div>

                            <!-- LAYER NR. 10 -->
                            <div class="tp-caption   tp-resizeme" id="slide-8-layer-3"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','15']"
                                 data-y="['top','top','top','top']" data-voffset="['20','29','32','28']"
                                 data-fontsize="['48','36','30','28']" data-lineheight="['70','50','40','36']"
                                 data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":"+770","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 9; white-space: nowrap; font-size: 48px; line-height: 70px; font-weight: 400; color: #262626; letter-spacing: 0px;">
                                Athric Care
                            </div>

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption   tp-resizeme" id="slide-8-layer-4"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['109','104','95','89']"
                                 data-fontsize="['24','24','24','20']" data-lineheight="['33','33','33','30']"
                                 data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":"+1380","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 10; white-space: nowrap; font-size: 24px; line-height: 33px; font-weight: 400; color: #262626;">
                            </div>

                            <!-- LAYER NR. 12 -->
                            <div class="tp-caption rev-btn " id="slide-8-layer-5" data-x="['left','left','left','center']"
                                 data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                                 data-voffset="['171','171','167','164']" data-width="none" data-height="none"
                                 data-whitespace="nowrap" data-type="button"
                                 data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"\/shop","delay":""}]'
                                 data-responsive_offset="on" data-responsive="off"
                                 data-frames='[{"delay":"+2170","speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(38,38,38);bw:2 2 2 2;"}]'
                                 data-margintop="[,,,]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[15,15,15,15]" data-paddingright="[45,45,45,45]"
                                 data-paddingbottom="[15,15,15,15]" data-paddingleft="[45,45,45,45]"
                                 style="z-index: 11; white-space: nowrap; font-size: 13px; line-height: 21px; font-weight: 400; color: #262626;background-color:rgba(38,38,38,0);border-color:rgb(38,38,38);border-style:solid;border-width:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                Shop Now
                            </div>
                        </div>

                        <!-- LAYER NR. 13 -->
                        <div class="tp-caption   tp-resizeme" id="slide-8-layer-8" data-x="['left','left','left','left']"
                             data-hoffset="['300','-50','-304','123']" data-y="['top','bottom','bottom','bottom']"
                             data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                             data-type="image" data-responsive_offset="on"
                             data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                             style="z-index: 5;">
                            <div class="rs-looped rs-pulse" data-easing="" data-speed="10" data-zoomstart="1"
                                 data-zoomend="1.1"><img src="{{URL::asset('assets/images/veg1.jpg')}}" alt=""
                                                         data-ww="['639px','464px','580px','440px']"
                                                         data-hh="['1060px','771px','963px','730px']" data-no-retina></div>
                        </div>

                        <!-- LAYER NR. 14 -->
                        <div class="tp-caption   tp-resizeme" id="slide-8-layer-6"
                             data-x="['center','center','center','center']" data-hoffset="['0','-91','-93','85']"
                             data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']"
                             data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                             data-basealign="slide" data-responsive_offset="on"
                             data-frames='[{"delay":2700,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                             style="z-index: 6;">
                            <div class="rs-looped rs-rotate" data-easing="Power0.easeIn" data-startdeg="-20"
                                 data-enddeg="20" data-speed="10" data-origin="50% 50%"><img
                                        src="assets/img/rev-slider/home1/new-m1-s2-2.png" alt=""
                                        data-ww="['623','427px','448px','323px']" data-hh="['260px','178px','187px','135px']"
                                        data-no-retina></div>
                        </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-9" data-transition="random-premium" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1=""
                        data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
                        data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/img/rev-slider/transparent.png" data-bgcolor='#f9f9f9'
                             style='background:#f9f9f9' alt="" data-bgposition="right center" data-bgfit="cover"
                             data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 15 -->
                        <div class="tp-caption     rev_group" id="slide-9-layer-1" data-x="['left','left','left','right']"
                             data-hoffset="['1080','457','324','-2']" data-y="['middle','middle','middle','middle']"
                             data-voffset="['0','-35','-23','-160']" data-width="['638','533','411','346']"
                             data-height="['240','236','225','225']" data-whitespace="nowrap" data-type="group"
                             data-responsive_offset="on"
                             data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                             data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             style="z-index: 7; min-width: 638px; max-width: 638px; max-width: 240px; max-width: 240px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                            <!-- LAYER NR. 16 -->
                            <div class="tp-caption   tp-resizeme" id="slide-9-layer-2"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none"
                                 data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                                 data-frames='[{"delay":"+290","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 8; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 700; color: #8d8d8d; letter-spacing: 2px;">
                            </div>

                            <!-- LAYER NR. 17 -->
                            <div class="tp-caption   tp-resizeme" id="slide-9-layer-3"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['20','29','32','28']"
                                 data-fontsize="['48','36','30','28']" data-lineheight="['70','50','40','36']"
                                 data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":"+770","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 9; white-space: nowrap; font-size: 48px; line-height: 70px; font-weight: 400; color: #262626; letter-spacing: 0px;">
                                Apple Cider
                            </div>

                            <!-- LAYER NR. 18 -->
                            <div class="tp-caption   tp-resizeme" id="slide-9-layer-4"
                                 data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['109','104','95','89']"
                                 data-fontsize="['24','24','24','20']" data-lineheight="['33','33','33','30']"
                                 data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":"+1380","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 10; white-space: nowrap; font-size: 24px; line-height: 33px; font-weight: 400; color: #262626; letter-spacing: 0px;">
                            </div>

                            <!-- LAYER NR. 19 -->
                            <div class="tp-caption rev-btn " id="slide-9-layer-5" data-x="['left','left','left','center']"
                                 data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                                 data-voffset="['171','171','167','164']" data-width="none" data-height="none"
                                 data-whitespace="nowrap" data-type="button"
                                 data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"\/shop","delay":""}]'
                                 data-responsive_offset="on" data-responsive="off"
                                 data-frames='[{"delay":"+2170","speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(38,38,38);bw:2 2 2 2;"}]'
                                 data-margintop="[,,,]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[15,15,15,15]" data-paddingright="[45,45,45,45]"
                                 data-paddingbottom="[15,15,15,15]" data-paddingleft="[45,45,45,45]"
                                 style="z-index: 11; white-space: nowrap; font-size: 13px; line-height: 21px; font-weight: 400; color: #262626;background-color:rgba(38,38,38,0);border-color:rgb(38,38,38);border-style:solid;border-width:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                Shop Now
                            </div>
                        </div>

                        <!-- LAYER NR. 20 -->
                        <div class="tp-caption   tp-resizeme" id="slide-9-layer-8" data-x="['left','left','left','left']"
                             data-hoffset="['300','-50','-304','-143']" data-y="['top','bottom','bottom','bottom']"
                             data-voffset="['0','0','0','-22']" data-width="none" data-height="none"
                             data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                             data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                             style="z-index: 5;">
                            <div class="rs-looped rs-pulse" data-easing="" data-speed="10" data-zoomstart="1"
                                 data-zoomend="1.1"><img src="{{URL::asset('assets/images/veg2.jpg')}}" alt=""
                                                         data-ww="['639px','464px','580px','440px']"
                                                         data-hh="['1060px','771px','963px','730px']" data-no-retina></div>
                        </div>

                        <!-- LAYER NR. 21 -->
                        <div class="tp-caption   tp-resizeme" id="slide-9-layer-6"
                             data-x="['center','center','center','center']" data-hoffset="['135','-6','28','49']"
                             data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','-4']"
                             data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                             data-basealign="slide" data-responsive_offset="on"
                             data-frames='[{"delay":2700,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                             style="z-index: 6;">
                            <div class="rs-looped rs-rotate" data-easing="Power0.easeIn" data-startdeg="-20"
                                 data-enddeg="20" data-speed="10" data-origin="50% 50%"><img
                                        src="assets/img/rev-slider/home1/new-m1-s3-2.png" alt=""
                                        data-ww="['701px','391px','422px','273px']" data-hh="['409px','228px','246px','159px']"
                                        data-no-retina></div>
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
    </div>
    <!--== End Slider Area ==-->

    <!--== Start Special Categories Banner ==-->
    <div class="special-category-banner pt-90 pt-md-60 pt-sm-50">
        <div class="container-fluid">
            <div class="special-category-banner-content">
                <div class="row">
                    <!-- Single Banner Start -->
                    <div class="col-sm-6 col-lg-4">
                        <div class="single-special-banner">
                            <figure class="banner-thumbnail">
                                <a href="{{ route('shop') }}"><img src="{{URL::asset('assets/images/nutri.jpg')}}" class="banner-thumb"
                                                         alt="Category Banner"/></a>
                                <figcaption class="banner-cate-name text-center">
                                    <h2 class="banner-title">Nutrition</h2>
                                    <a href="{{ route('shop') }}" class="btn btn-black">Shop Now</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <!-- Single Banner End -->

                    <!-- Single Banner Start -->
                    <div class="col-sm-6 col-lg-4">
                        <div class="single-special-banner">
                            <figure class="banner-thumbnail">
                                <a href="{{ route('shop') }}"><img src="{{URL::asset('assets/images/skin.jpg')}}" class="banner-thumb"
                                                         alt="Category Banner"/></a>
                                <figcaption class="banner-cate-name text-center">
                                    <h2 class="banner-title">Beauty Care</h2>
                                    <a href="{{ route('shop') }}" class="btn btn-black">Shop Now</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <!-- Single Banner End -->

                    <!-- Single Banner Start -->
                    <div class="col-sm-6 col-lg-4">
                        <div class="single-special-banner">
                            <figure class="banner-thumbnail">
                                <a href="{{ route('shop') }}"><img src="{{URL::asset('assets/images/green-tea.png')}}" class="banner-thumb"
                                                         alt="Category Banner"/></a>
                                <figcaption class="banner-cate-name text-center">
                                    <h2 class="banner-title">Supplements</h2>
                                    <a href="{{ route('shop') }}" class="btn btn-black">Shop Now</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <!-- Single Banner End -->
                </div>
            </div>
        </div>
    </div>
    <!--== End Special Categories Banner ==-->

    <!-- Start Must Have Products Area -->
    <section id="mustHave-products-area" class="pt-90 pt-md-60 pt-sm-50">
        <div class="container-fluid">
            <div class="row">
                <!-- Start Section title -->
                <div class="col-lg-8 m-auto text-center">
                    <div class="section-title-wrap">
                        <h2>Newly Arrived</h2>
                    </div>
                </div>
                <!-- End Section title -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="products-wrapper">
                        <div class="product-carousel-wrap">
                @foreach($prod as $product)
                    <!-- Start Single Product -->
                        <div class="single-product-wrap">
                            <!-- Product Thumbnail -->
                            <figure class="product-thumbnail">
                                <a href="single-product.html" class="d-block">
                                    <img class="primary-thumb" src="{{ URL::asset('assets/images/').'/'.$product->productImage }}" alt="Product"/>
                                    <img class="secondary-thumb" src="{{ URL::asset('assets/images/').'/'.$product->secondImage }}" alt="Product"/>
                                </a>
                                <figcaption class="product-hvr-content">
                                    <a href="#" class="btn btn-black btn-addToCart addToCart">
                                        Add to Cart
                                        <span class="productID" style="display: none;">{{ $product->id }}</span></a>
                                    <div class="prod-btn-group">
                                        @php
                                            $name = explode(" ", $product->productName);
                                        @endphp
                                        <span data-toggle="tooltip" data-placement="top" title="Quick Shop"><button
                                                    data-toggle="modal" data-target="#{{ $name[0] }}"><i
                                                        class="dl-icon-view"></i></button></span>
                                    </div>
                                    <span class="product-badge">New</span>

                                </figcaption>
                            </figure>

                            <!-- Product Details -->
                            <div class="product-details">
                                <h2 class="product-name"><a href="single-product.html">{{ $product->productName }}</a></h2>
                                <div class="product-prices">
                                    <span class="price">₦{{ $product->productPrice }}</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Product -->

                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Must Have Products Area -->

    <!-- Start Best Seller Products Area -->
    <section id="bestSeller-area-wrapper" class="pt-90 pt-md-60 pt-sm-50">
        <div class="container-fluid">
            <div class="row">
                <!-- Start Section title -->
                <div class="col-lg-8 m-auto text-center">
                    <div class="section-title-wrap">
                        <h2>Popular</h2>
                    </div>
                </div>
                <!-- End Section title -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="products-wrapper">
                        <div class="product-carousel-wrap">
                            <!-- Start Single Product -->
                        @foreach($prod as $product)
                            <!-- Start Single Product -->
                                <div class="single-product-wrap">
                                    <!-- Product Thumbnail -->
                                    <figure class="product-thumbnail">
                                        <a href="single-product.html" class="d-block">
                                            <img class="primary-thumb" src="{{ URL::asset('assets/images/').'/'.$product->productImage }}" alt="Product"/>
                                            <img class="secondary-thumb" src="{{ URL::asset('assets/images/').'/'.$product->secondImage }}" alt="Product"/>
                                        </a>
                                        <figcaption class="product-hvr-content">
                                            <a href="#" class="btn btn-black btn-addToCart addToCart">
                                                Add to Cart
                                                <div class="productID" style="display: none;">{{ $product->id }}</div>
                                            </a>
                                            <div class="prod-btn-group">

                                                @php
                                                    $name = explode(" ", $product->productName);
                                                @endphp
                                                <span data-toggle="tooltip" data-placement="top" title="Quick Shop"><button
                                                            data-toggle="modal" data-target="#{{ $name[0] }}"><i
                                                                class="dl-icon-view"></i></button></span>
                                            </div>
                                            <span class="product-badge">Popular</span>

                                        </figcaption>
                                    </figure>

                                    <!-- Product Details -->
                                    <div class="product-details">
                                        <h2 class="product-name">
                                            <a href="single-product.html">{{ $product->productName }}</a>
                                        </h2>
                                        <div class="product-prices">
                                            <span class="price">₦{{ $product->productPrice }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->

                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Best Seller Products Area -->

    <!--== Start Instagram Feed Area ==-->
    <div class="instagram-feed-area pt-86 pt-md-58 pt-sm-46">
        <div class="instagram-content-header text-center">
            <h3>Instagram <span>@iandyourhealth</span></h3>
        </div>

        <div class="instagram-feed-thumb">
            <div id="instafeed" class="instagram-carousel" data-userid="8774663968"
                 data-accesstoken="8774663968.1677ed0.78a087bca56440759bdd9ed8f26e2aac">
            </div>
        </div>
    </div>
    <!--== End Instagram Feed Area ==-->




@endsection
