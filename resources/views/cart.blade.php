@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <br><br><br><br>

    <!--== Start Cart Page Wrapper ==-->
    <div id="cart-page-wrapper" class="pt-86 pt-md-56 pt-sm-46 pb-50 pb-md-20 pb-sm-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="shopping-cart-list-area">
                        <div class="shopping-cart-table table-responsive">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th>Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>

                                <tbody>



                                @if(\Cart::session(session()->get('_token'))->getContent()->count())

                                    @foreach(\Cart::session(session()->get('_token'))->getContent() as $cartItems)

                                        <tr>
                                            <td class="product-list">
                                                <div class="cart-product-item d-flex align-items-center">
                                                    <div class="remove-icon">
                                                        <button onclick="removeCart({{ $cartItems->id }})"><i class="fa fa-trash-o"></i></button>
                                                    </div>
                                                    <a href="#" class="product-thumb">
                                                        <img src="{{ URL::asset('assets/images/').'/'.$cartItems->attributes['image'] }}" alt="{{ $cartItems->name }}"/>
                                                    </a>
                                                    <a href="single-product-tab-left.html" class="product-name">{{ $cartItems->name }}</a>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="price">₦{{ $cartItems->price }}</span>
                                            </td>
                                            <td>
                                                <div class="pro-qty">
                                                    <input type="text" class="quantity" value="{{ $cartItems->quantity }}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="price">₦{{ $cartItems->price }}</span>
                                            </td>
                                        </tr>



                                    @endforeach

                                @else
                                    <tr>
                                        <td class="product-list">
                                            <div class="cart-product-item d-flex align-items-center">
                                                <div class="remove-icon">
                                                    <button><i class="fa fa-trash-o"></i></button>
                                                </div>
                                                <span>Empty</span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="price">Empty</span>
                                        </td>
                                        <td>
                                            <div class="pro-qty">
                                                <span>0</span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="price">Empty</span>
                                        </td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>

                        <div class="cart-coupon-update-area d-sm-flex justify-content-between align-items-center">

                            <div class="cart-update-buttons mt-xs-14">
                                <button class="btn-clear-cart" onclick="clearCart()">Clear Cart</button>
                                <button class="btn-update-cart">Update Cart</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <!-- Cart Calculate Area -->
                    <div class="cart-calculate-area mt-sm-30 mt-md-30">
                        <h5 class="cal-title">Cart Totals</h5>

                        <div class="cart-cal-table table-responsive">
                            <table class="table table-borderless">
                                <tr class="cart-sub-total">
                                    <th>Subtotal</th>
                                    <td>₦ {{ \Cart::session(session()->get('_token'))->getSubTotal() }}</td>
                                </tr>

                                <tr class="order-total">
                                    <th>Total</th>
                                    <td><b>₦ {{ \Cart::session(session()->get('_token'))->getTotal() }}</b></td>
                                </tr>
                            </table>
                        </div>

                        <div class="proceed-checkout-btn">
                            <a href="{{ route('checkout') }}" class="btn btn-full btn-black">Proceed to Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Cart Page Wrapper ==-->

@endsection